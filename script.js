const questionTextElement = $("#questionText")
const formContentElement = $("#formContent")
var id = 1
$("#addQuestion").click(function () {
    const newId = id
    const question = questionTextElement.val()
    const questionElement = $(`<div>
<h2>${question}</h2>
<div class="responses"></div>
<input type="text" class="response-input">
<button class="add-response">+</button>
</div>`)
    questionElement.find(".add-response").click(function () {
        const response = questionElement.find('.response-input').val()
        const responseElement = $(`<label><input type="radio" name="${newId}-input">${response}</label>`)
        questionElement.find(".responses").append(responseElement)
    })
    formContentElement.append(questionElement)
    id++
})
